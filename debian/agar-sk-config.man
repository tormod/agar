.TH agar\-sk\-config 1 "August 8, 2020" "" "agar"

.SH NAME
agar\-sk\-config \- get package configuration for libag\-sk 

.SH SYNOPSIS
.B agar\-sk\-config
[\-\-prefix[=DIR]] [\-\-exec\-prefix[=DIR]] [\-\-version] [\-\-cflags] [\-\-libs]

.SH OPTIONS
.IP \-\-prefix[=DIR]

.IP \-\-exec\-prefix[=DIR]

.IP \-\-version

.IP \-\-cflags
This prints preprocessor flags required to compile code using the libag\-sk
package on the command line.  Suitable for use in a CPPFLAGS variable in a
Makefile.

.IP \-\-libs
Prints the linker flags required to link objects using libag\-sk.

.SH DESCRIPTION

Gets the configuration information necessary to use the Agar sk library for
application development.
