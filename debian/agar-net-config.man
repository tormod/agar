.TH agar\-net\-config 1 "August 8, 2020" "" "agar"

.SH NAME
agar\-net\-config \- get package configuration for libag\-net 

.SH SYNOPSIS
.B agar\-net\-config
[\-\-prefix[=DIR]] [\-\-exec\-prefix[=DIR]] [\-\-version] [\-\-cflags] [\-\-libs]

.SH OPTIONS
.IP \-\-prefix[=DIR]

.IP \-\-exec\-prefix[=DIR]

.IP \-\-version

.IP \-\-cflags
This prints preprocessor flags required to compile code using the libag\-net
package on the command line.  Suitable for use in a CPPFLAGS variable in a
Makefile.

.IP \-\-libs
Prints the linker flags required to link objects using libag\-net.

.SH DESCRIPTION

Gets the configuration information necessary to use the Agar net library for
application development.
