.TH agar\-sg\-config 1 "August 8, 2020" "" "agar"

.SH NAME
agar\-sg\-config \- get package configuration for libag\-sg 

.SH SYNOPSIS
.B agar\-sg\-config
[\-\-prefix[=DIR]] [\-\-exec\-prefix[=DIR]] [\-\-version] [\-\-cflags] [\-\-libs]

.SH OPTIONS
.IP \-\-prefix[=DIR]

.IP \-\-exec\-prefix[=DIR]

.IP \-\-version

.IP \-\-cflags
This prints preprocessor flags required to compile code using the libag\-sg
package on the command line.  Suitable for use in a CPPFLAGS variable in a
Makefile.

.IP \-\-libs
Prints the linker flags required to link objects using libag\-sg.

.SH DESCRIPTION

Gets the configuration information necessary to use the Agar sg library for
application development.
